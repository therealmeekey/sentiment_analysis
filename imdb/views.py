from django.shortcuts import render, get_object_or_404, redirect
from imdb.models import Review
from imdb.forms import ReviewForm
from imdb.analysis import print_result, analyse_text, vectorizer, classifier


def review_list(request):
    reviews = Review.objects.all().order_by('-id')
    return render(request, 'imdb/reviews_list.html', {'reviews': reviews})


def review_new(request):
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(commit=False)
            review.save()
            review.result = print_result(analyse_text(classifier, vectorizer, review.review))
            review.save()
            return redirect('review_list')
    else:
        form = ReviewForm()
    return render(request, 'imdb/create_reviews.html', {'form': form})
