from django import forms
from imdb.models import Review, Movie


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ('review', 'movie')

    def __init__(self, *args, **kwargs):
        super(ReviewForm, self).__init__(*args, **kwargs)
        self.fields['review'].widget.attrs.update({'class': 'materialize-textarea'})
        self.fields['movie'].queryset = Movie.objects.all()
