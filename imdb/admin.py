from django.contrib import admin
from imdb.models import Review, Movie
from django.contrib.auth.models import User, Group


class ReviewInline(admin.TabularInline):
    model = Review
    insert_after = 'image'
    extra = 1


class MovieAdmin(admin.ModelAdmin):
    inlines = [ReviewInline]


admin.site.site_header = 'Сайт Администратора'
admin.site.site_title = 'Сайт Администратора'
admin.site.index_title = 'Администратор'

admin.site.register(Movie, MovieAdmin)
admin.site.unregister(User)
admin.site.unregister(Group)
