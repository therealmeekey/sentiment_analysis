from django.apps import AppConfig


class ImdbConfig(AppConfig):
    name = 'imdb'
    verbose_name = 'Анализ отзывов'
