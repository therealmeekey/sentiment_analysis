from django.db import models


class Movie(models.Model):
    name = models.CharField('Фильм', max_length=128, blank=True, null=True)
    description = models.TextField('Описание', blank=True, null=True)
    image = models.ImageField('Обложка', blank=True, null=True, upload_to='movie_image',
                              default='movie_images/default.png',
                              )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Фильм'
        verbose_name_plural = 'Фильмы'


class Review(models.Model):
    REVIEW_TYPE = (
        ('Positive', 'Положительный'),
        ('Negative', 'Негативный')
    )

    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    review = models.TextField('Отзыв', blank=True, null=True)
    result = models.CharField('Результат', choices=REVIEW_TYPE, max_length=32, blank=True, null=True)
    created = models.DateField('Дата создания', auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return '{}'.format(self.result)

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
        ordering = ['-created']
